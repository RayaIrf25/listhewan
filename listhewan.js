const listHewan = [
    {
        nama : "Paus",
        berkembangbiak : "Vivipar"
    },
    {
        nama: "Kucing",
        berkembangbiak: "Vivipar"
    },
    {
        nama: "Komodo",
        berkembangbiak: "Ovipar"
    },
    {
        nama: "Ular",
        berkembangbiak: "Ovovivipar"
    },
    {
        nama: "Ayam",
        berkembangbiak: "Ovipar"
    },
    {
        nama: "Burung",
        berkembangbiak: "Ovipar"
    },
]

for (let i = 0; i < listHewan.length; i++) {
    console.log(listHewan[i]["nama"],
        ("berkembangbiak " + listHewan[i]["berkembangbiak"]));
}

console.log("-cari-")
function cariHewan(nama) {
    for(let i = 0; i < listHewan.length; i++) {
        if (listHewan[i]["nama"] == nama) {
            console.log(listHewan[i]["nama"], ("berkembangbiak " + listHewan[i]["berkembangbiak"]));
        }
    }
}
cariHewan("Ular");

console.log("-filter-");
function filterHewan(berkembangbiak) {
    for(let i = 0; i < listHewan.length; i++){
        if (listHewan[i]["berkembangbiak"] == berkembangbiak){
            console.log(listHewan[i]["nama"]);
        }
    }
}
filterHewan("Ovipar")